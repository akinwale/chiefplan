<?php

namespace App\Model\Table;

use Cake\ORM\Table;

class PlansTable extends Table {
    public function initialize(array $config) {
        parent::initialize($config);

        $this->table('Plans');
        $this->primaryKey('Id');

        $this->addBehavior('SimpleAudit');

        $this->addAssociations([
            'belongsTo' => [
                'Creator' => [
                    'className'     => 'App\Model\Table\UsersTable',
                    'foreignKey'    => 'UserId',
                    'propertyName'  => 'Creator'
                ]
            ],

            'belongsToMany' => [
                'Participants' => [
                    'className'         => 'App\Model\Table\UsersTable',
                    'joinTable'         => 'PlanParticipants',
                    'foreignKey'        => 'PlanId',
                    'targetForeignKey'  => 'UserId',
                    'propertyName'      => 'PlanParticipants',
                    'unique'            => true
                ]
            ]
        ]);
    }
}

?>