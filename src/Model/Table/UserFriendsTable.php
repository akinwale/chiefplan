<?php

namespace App\Model\Table;

use Cake\ORM\Table;

class UserFriendsTable extends Table {
    public function initialize(array $config) {
        parent::initialize($config);

        $this->table('UserFriends');
        $this->primaryKey('Id');

        $this->addBehavior('SimpleAudit');
    }
}

?>