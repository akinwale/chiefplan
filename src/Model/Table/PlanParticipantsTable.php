<?php

namespace App\Model\Table;

use Cake\ORM\Table;

class PlanParticipantsTable extends Table {
    public function initialize(array $config) {
        parent::initialize($config);

        $this->table('PlanParticipants');
        $this->primaryKey('Id');

        $this->addBehavior('SimpleAudit');
    }
}

?>