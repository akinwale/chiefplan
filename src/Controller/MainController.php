<?php

namespace App\Controller;

use Cake\Datasource\ConnectionManager;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;


// Google+ API Client ID
// 48655955495-io5d8dd1lgb4rghnb1f8fk0emphicpa4.apps.googleusercontent.com

// Google+ API Client Secret
// qqseb2jwVqlp328fI3j3cYWR

class MainController extends AppController {
    private $_clientId = '48655955495-io5d8dd1lgb4rghnb1f8fk0emphicpa4.apps.googleusercontent.com';

    protected $client;

    protected $plus;

    protected $calService;

    protected $currentUser;

    private $session;

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);

        $this->session = $this->request->session();

        $this->client = new \Google_Client();
        $this->client->setApplicationName('Chief Plan');
        $this->client->setClientId($this->_clientId);
        $this->client->setClientSecret('qqseb2jwVqlp328fI3j3cYWR');
        $this->client->setRedirectUri('http://www.chiefplan.com');

        $this->plus = new \Google_Service_Plus($this->client);

        $this->calService = new \Google_Service_Calendar($this->client);

        $this->loadModel('Users');
        $this->loadModel('UserFriends');
        $this->loadModel('Plans');
        $this->loadModel('PlanParticipants');

        $accessToken = $this->session->read('token');
        if (!empty($accessToken)) {
            $this->set('gplusToken', $accessToken);
            $this->client->setAccessToken($accessToken);

            // Make sure the user object exists in the DB
            $peopleCurrent = $this->plus->people->get('me');
            if ($peopleCurrent) {
                $currentPerson = $peopleCurrent->toSimpleObject();

                $emailList = $currentPerson->emails;
                $email = (count($emailList) > 0) ?
                    ($emailList[0]['value']) : '';

                $personData = [
                    'GooglePlusId' => $currentPerson->id,
                    'Name' => trim(sprintf('%s %s',
                                           @$currentPerson->name['givenName'],
                                           @$currentPerson->name['familyName'])),
                    'Email' => $email,
                    'ImageUrl' => @$currentPerson->image['url']
                ];

                $userTable = TableRegistry::get('Users');
                $cpUser = $userTable->find('all', [
                    'conditions' => ['GooglePlusId' => $currentPerson->id]
                ])->first();
                if (!$cpUser) {
                    $userEntity = $userTable->newEntity($personData);
                    if ($userTable->save($userEntity)) {
                        $cpUser = $userEntity;
                    }
                }

                $this->currentUser = $cpUser;
                $this->set('currentUser', $cpUser);
            }

            if ($this->request->action == 'index') {
                return $this->redirect('/main/plans');
            }
        }
    }

    public function landing() {
        return $this->redirect('http://www.chiefplan.com/main');
    }

    // Sign in with Google+
    public function index() {

    }

    protected function _jsonResponse($object = []) {
        $this->autoRender = false;

        $this->response->type('json');
        $this->response->body(json_encode($object));
    }

    public function createplan() {
        if ($this->request->is('post')) {
            $planTable = TableRegistry::get('Plans');
            $planData = $this->request->data;

            $selFriendIds = @$this->request->data['selectedFriends'];
            $allParticipantIds = [];
            $allParticipantIds[] = $this->currentUser->Id;
            if (is_array($selFriendIds)) {
                foreach ($selFriendIds as $friendId) {
                    if (!in_array($friendId, $allParticipantIds)) {
                        $allParticipantIds[] = $friendId;
                    }
                }
            }

            // Get the number of payers
            $splitPayers = @$this->request->data['splitPayers'];
            $numSplitPayers = is_array($splitPayers) ? count($splitPayers) : 0;
            $budget = doubleval($this->request->data['PlanCost']);
            $splitAmount = ($numSplitPayers > 0) ? ($budget / $numSplitPayers) : $budget;

            $allParticipantsData = [];
            foreach ($allParticipantIds as $participantId) {
                $data = ['Id' => $participantId];
                $allParticipantsData[] = $data;
            }

            $planData['PlanParticipants'] = $allParticipantsData;
            $plan = $planTable->newEntity($planData, ['associated' => ['Participants']]);

            $plan->PlanTime = $this->request->data['PlanTime'];
            $plan->UserId = $this->currentUser->Id;

            $dbError = false;

            $conn = ConnectionManager::get('default');
            $conn->begin();
            if ($planTable->save($plan)) {
                foreach ($allParticipantIds as $participantId) {
                    if ($numSplitPayers > 0 && in_array($participantId, $splitPayers)) {
                        $conn->query("UPDATE PlanParticipants SET SplitAmount = $splitAmount WHERE PlanId = {$plan->Id} AND UserId = $participantId");
                    } else if ($participantId == $this->currentUser->Id) {
                        $conn->query("UPDATE PlanParticipants SET SplitAmount = $splitAmount WHERE PlanId = {$plan->Id} AND UserId = $participantId");
                    }
                }
            } else {
                $dbError = true;
                $conn->rollback();
            }
            $conn->commit();

            if ($dbError) {
                return $this->_jsonResponse(['error' => true,
                                             'message' => 'Unable to create the plan']);
            } else {
                // Create the calendar event
                $userTable = TableRegistry::get('Users');
                $userParticipants = $userTable->find('all')->
                    where(['Id IN' => $allParticipantIds])->all();
                $attendees = [];
                foreach ($userParticipants as $participant) {
                    $attendees[] = ['email' => $participant->Email];
                }
                $startDt = str_replace(' ', 'T', $this->request->data['PlanTime']) . 'Z';
                $startDtObj = new \DateTime($startDt);
                $endDtObj = $startDtObj->add(new \DateInterval('PT3H')); // add a 3-hour interval for end date
                $event = new \Google_Service_Calendar_Event(array(
                    'summary' => $plan->Title,
                    'location' => $plan->LocationName,
                    'description' => $plan->Description,
                    'start' => array(
                        'dateTime' => $startDt,
                        'timeZone' => 'UTC',
                    ),
                    'end' => array(
                        'dateTime' => $endDtObj->format('Y-m-d\TH:i:s\Z'),
                        'timeZone' => 'UTC',
                    ),
                    'attendees' => $attendees,
                    'reminders' => array(
                      'useDefault' => false,
                      'overrides' => array(
                        array('method' => 'email', 'minutes' => 24 * 60),
                        array('method' => 'popup', 'minutes' => 10),
                      ),
                    ),
                ));

                $calendarId = 'primary';
                $event = $this->calService->events->insert($calendarId, $event);

                return $this->_jsonResponse(['success' => true,
                                             'result' => $plan->Id]);
            }
        }

        return $this->_jsonResponse(['error' => true,
                                     'message' => 'Invalid request.']);
    }

    public function plans() {
        $planTable = TableRegistry::get('Plans');
        $currentUserId = $this->currentUser->Id;
        $plans = $planTable->find('all', ['contain' => ['Creator', 'Participants']])->
            where(["Plans.Id IN (SELECT DISTINCT P.Id FROM Plans P JOIN PlanParticipants PP ON PP.PlanId = P.Id WHERE P.UserId = $currentUserId OR PP.UserId = $currentUserId)"])->all();
        $this->set('plans', $plans);
    }

    public function planner() {
        // Check if the user has friends stored
        $friendTable = TableRegistry::get('UserFriends');
        $cpUserFriends = $friendTable->find()->
            where(['UserId' => $this->currentUser->Id])->all()->toArray();

        //if (count($cpUserFriends) == 0) {
        // Retrieve remote friends
        $people = $this->plus->people->listPeople('me', 'visible', array());
        $peopleObjects = $people->toSimpleObject();
        if ($peopleObjects->items) {
            foreach ($peopleObjects->items as $gplusFriend) {
                if ($gplusFriend['objectType'] != 'person') {
                    continue;
                }

                // Check if the person exists
                $existFriend = $friendTable->find('all')->where(
                    ['GooglePlusId' => $gplusFriend['id'],
                     'UserId' => $this->currentUser->Id])->first();
                if ($existFriend) {
                    continue;
                }

                $friendData = [
                    'GooglePlusId' => $gplusFriend['id'],
                    'UserId' => $this->currentUser->Id,
                    'Name' => $gplusFriend['displayName'],
                    'ImageUrl' => $gplusFriend['image']['url']
                ];
                $cpFriend = $friendTable->newEntity($friendData);
                    $friendTable->save($cpFriend);
            }
            $conn = ConnectionManager::get('default');
            $conn->query('UPDATE UserFriends UF SET UF.FriendId = (SELECT U.Id FROM Users U WHERE U.GooglePlusId = UF.GooglePlusId) WHERE UF.FriendId IS NULL');
            $cpUserFriends = $friendTable->find('all', ['conditions' => ['UserId' => $this->currentUser->Id]])->all();
        }
        //}
        $this->set('friends', $cpUserFriends);
    }

    public function updatetoken() {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $this->session->write('token', $this->request->data['token']);
        }
    }

    // Google+ connect
    public function connect() {
        $this->autoRender = false;

        if ($this->request->is('post')) {
            $token = $this->session->read('token');
            if (empty($token)) {
                $code = file_get_contents('php://input');
                $client->authenticate($code);
                $token = json_decode($client->getAccessToken());

                $attributes = $client->verifyIdToken($token->id_token, $this->_clientId)
                    ->getAttributes();
                $gplus_id = $attributes["payload"]["sub"];
                // Store the token in the session for later use.
                $app['session']->set('token', json_encode($token));
                $response = 'Successfully connected with token: ' . print_r($token, true);
            }
        }
    }

    public function people() {
        $this->autoRender = false;

        $accessToken = $this->session->read('token');
        if (empty($accessToken)) {
            return;
        }

        $people = $this->plus->people->listPeople('me', 'visible', array());
    }
}

?>