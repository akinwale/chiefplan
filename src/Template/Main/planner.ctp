<h3>Plan your group event</h3>
    <h4>Who's going? (Step 1 of 2) <div><small><em>You can only select friends who are using Chief Plan.</em></small></div></h4>
<div class="friend-select">
    <?php foreach ($friends as $friend): ?>
    <div id="<?php echo $friend->GooglePlusId ?>" class="friend-item">
        <img src="<?php echo $friend->ImageUrl ?>" alt="" />
        <span><?php echo $friend->Name ?>
        <?php if ($friend->FriendId > 0): ?><br /><small>uses Chief Plan</small><?php endif; ?></span>
    </div>
    <?php endforeach; ?>
    <div class="clear"></div>
</div>

<div class="outing-details" style="display: none">
    <h4>Organise (Step 2 of 2)</h4>
    <form role="form" class="form-horizontal">
        <div class="form-group">
            <label class="control-label col-md-2">What's happening?</label>
            <div class="col-md-10">
                <input type="text"
                       class="form-control"
                       name="OutingTitle"
                       id="OutingTitle" />
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-2">Where?</label>
            <div class="col-md-4">
                <input type="text"
                       class="form-control"
                       name="OutingLocation"
                       id="OutingLocation" />
            </div>

            <label class="control-label col-md-2">When?</label>
            <div class="col-md-4">
                <input type="text"
                       class="form-control"
                       name="OutingTime"
                       id="OutingTime" />
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-2">Details</label>
            <div class="col-md-10">
                <textarea
                       class="form-control"
                       name="OutingDesc"
                       id="OutingDesc"></textarea>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-2">How much?</label>
            <div class="col-md-4">
                <div class="input-group">
                    <span class="input-group-addon">$</span>
                    <input type="text"
                        class="form-control"
                        name="OutingBudget"
                        id="OutingBudget" />
                </div>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-2">Who's Paying?</label>
            <div class="col-md-4 outing-participant-payers">
                <div class="outing-payer">
                    <input type="checkbox" class="outing-payer-check" name="outing-participant-payer[]" value="<?php echo $currentUser->Id ?>" checked="checked" /> <span>Myself</span> <span class="split-budget"></span>
                </div>
                <div class="other-outing-payers">

                </div>
            </div>
        </div>
    </form>
</div>

<div class="buttons row">
    <div class="col-md-6">
        <button id="back-button" class="btn btn-lg btn-default">Back</button>
    </div>
    <div class="col-md-6 text-right">
        <button id="submit-button" class="btn btn-lg btn-primary">Next</button>
    </div>
</div>

<script type="text/javascript">
var currentView = null;
var currentFriends = {};
<?php foreach ($friends as $friend): ?>
currentFriends['<?php echo $friend->GooglePlusId ?>'] = {
    'Id': <?php echo $friend->Id ?>,
    'FriendId': '<?php echo $friend->FriendId ?>',
    'Name': "<?php echo $friend->Name ?>"
};
<?php endforeach; ?>
$(document).ready(function() {
    $('#OutingTime').datetimepicker({format: 'YYYY-MM-DD HH:mm:ss'});
    currentView = $('.friend-select').is(':visible') ? 'friendSelect' : 'organise';

    $('.friend-item').on('click', function(evt) {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
        } else {
            var friendGplusId = $(this).attr('id');
            var friendObject = currentFriends[friendGplusId];
            if (friendObject) {
                var friendId = parseInt(friendObject['FriendId']);
                if (isNaN(friendId) || friendId == 0) {
                    showAlertDialog(friendObject['Name'] + ' is not on Chief Plan!',
                                    'Your friend, ' + friendObject['Name'] + ' is not using Chief Plan yet. Invite them to be able to add them to your group events!');
                    return;
                }
            }

            $(this).addClass('active');
        }
    });

    $('.modal-alert-dismiss').on('click', function(evt) {
       $(this).parents('.modal').modal('hide');
    });

    $('#back-button').on('click', function(evt) {
        evt.preventDefault();

        if (currentView == 'friendSelect') {
            window.location.href = '/main/plans';
        } else if (currentView == 'organise') {
            $('.friend-select').css('display', 'block');
            $('.outing-details').css('display', 'none');
            $('#submit-button').text('Next');
            currentView = 'friendSelect';
        }
    });

    $('#submit-button').on('click', function(evt) {
        evt.preventDefault();

        if (currentView == 'friendSelect') {
            onSubmitFriendSelect();
        } else if (currentView == 'organise') {
            onSubmitOrganise();
        }
    });

    $(document).on('change blur', '#OutingBudget', onBudgetChanged);
    $(document).on('change', '.outing-payer-check', onPayerCheckChanged);
    $('.modal-alert').on('shown.bs.modal', onAlertDialogShown);
    $('.modal-alert').on('hidden.bs.modal', onAlertDialogHidden);
});

var onSubmitFriendSelect = function() {
    // Check selected friends
    var selectedFriends = [];
    $('.friend-item').each(function() {
        var id = $(this).attr('id');
        if ($(this).hasClass('active')) {
            selectedFriends.push(currentFriends[id]);
        }
    });

    if (selectedFriends.length == 0) {
        showAlertDialog('No friend selected',
                        'You need to select at least one friend to continue.');
        return;
    }

    // Can display "Organise" page
    $('.other-outing-payers').html('');
    for (var i = 0; i < selectedFriends.length; i++) {
        var payerDiv = $('<div></div>').attr('class', 'outing-payer').append(
            $('<input />').attr({'type': 'checkbox',
                                'class': 'outing-payer-check',
                                'name': 'outing-participant-payer[]',
                                'value': selectedFriends[i]['FriendId']})
        ).append(
            $('<span></span>').text(' ' + selectedFriends[i]['Name'])
        ).append(
            $('<span></span>').attr('class', 'split-budget')
        );
        $('.other-outing-payers').append(payerDiv);
    }

    $('.friend-select').css('display', 'none');
    $('.outing-details').css('display', 'block');
    $('#submit-button').text("Let's do this!");
    currentView = 'organise';
};

var onSubmitOrganise = function() {
    // Validate
    var postData = {
        'Title': $('#OutingTitle').val(),
        'Description': $('#OutingDesc').val(),
        'LocationName': $('#OutingLocation').val(),
        'PlanTime': $('#OutingTime').val(),
        'PlanCost': $('#OutingBudget').val()
    }

    if ($.trim(postData['Title']).length == 0) {
        showAlertDialog('Missing information', 'Please enter what\'s happening for your group event.');
        return;
    }
    if ($.trim(postData['LocationName']).length == 0) {
        showAlertDialog('Missing location', 'Please enter a location for your group event.');
        return;
    }
    if ($.trim(postData['PlanTime']).length == 0) {
        showAlertDialog('Missing time', 'Please select the time for your group event.');
        return;
    }
    var budgetValue = parseInt($('#OutingBudget').val());
    if (isNaN(budgetValue)) {
        showAlertDialog('Invalid budget specified', 'Please specify a valid budget for your group event.');
        return;
    }

    var selectedFriends = [];
    $('.friend-item').each(function() {
        var id = $(this).attr('id');
        if ($(this).hasClass('active')) {
            selectedFriends.push(currentFriends[id]);
        }
    });
    var selFriendIds = [];
    var splitPayerIds = [];
    for (var i = 0; i < selectedFriends.length; i++) {
        selFriendIds.push(selectedFriends[i]['FriendId']);
    }

    $('.outing-payer').each(function() {
        var cbx = $(this).find('input[type="checkbox"]');
        if (cbx.is(':checked')) {
            splitPayerIds.push(cbx.val());;
        }
    });


    postData['selectedFriends'] = selFriendIds;
    postData['splitPayers'] = splitPayerIds;
    console.log(postData);

    // Then submit
    $.ajax({
        type: 'post',
        data: postData,
        dataType: 'json',
        url: '/main/createplan',
        success: function(res) {
            if (res && res.success) {
                window.location.href = '/main/plans?created';
            }
        },
        error: function(err) {
            showAlertDialog('Unable to create plan',
                            'An error occurred trying to process your request. Please check your Internet connection and try again.');
        },
        complete: function() {

        }
    });
}

var showAlertDialog = function(title, message) {
    $('.modal-alert').find('.modal-title').text(title);
    $('.modal-alert').find('.modal-body').text(message);
    $('.modal-alert').modal('show');
}

var onBudgetChanged = function(evt) {
    var budget = parseInt($('#OutingBudget').val());
    if (isNaN(budget)) {
        return;
    }

    updatePayerBudgets(budget);
}

var onPayerCheckChanged = function() {
    var budget = parseInt($('#OutingBudget').val());
    if (isNaN(budget)) {
        return;
    }

    updatePayerBudgets(budget);
}

var updatePayerBudgets = function(budgetValue) {
    var numChecked = 0;
    $('.outing-payer').each(function() {
        var cbx = $(this).find('input[type="checkbox"]');
        if (cbx.is(':checked')) {
            numChecked++;
        }
    });
    if (numChecked <= 0) {
        return;
    }
    var splitBudget = budgetValue / numChecked;
    $('.outing-payer').each(function() {
        var cbx = $(this).find('input[type="checkbox"]');
        var budgetSpan = $(this).find('span.split-budget');
        if (cbx.is(':checked')) {
            budgetSpan.text(' ($' + splitBudget.toFixed(2) + ')');
        } else {
            budgetSpan.text('');
        }
    });
}

var onAlertDialogShown = function(evt) {

}

var onAlertDialogHidden = function(evt) {

}
</script>
