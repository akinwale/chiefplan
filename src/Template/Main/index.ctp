<?php
$this->start('gapi');
?>

<?php
$this->end('gapi');
?>

<div class="row" style="padding-top: 120px">
    <div class="col-md-6 col-md-offset-3">
        <h2>Welcome to Chief Plan</h2>
            <div>Organise outings with your friends. Give them a treat by taking them to dinner, the movies, or concerts. Or make plans to split the bill.</div>

            <h3>Sign in with Google+ to get started.</h3>
            <div>
                <div id="gConnect">
                    <img
                        class="gPlusButton"
                        id="gPlusButton"
                        src="/img/gplus_signin_button.png"
                        alt="Sign in with Google+" />
                </div>

            </div>
    </div>
</div>

<script type="text/javascript">
var auth2 = auth2 || {};
$(document).ready(function() {
    (function() {
        var po = document.createElement('script');
        po.type = 'text/javascript'; po.async = true;
        po.src = 'https://plus.google.com/js/client:plusone.js?onload=startApp';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(po, s);
    })();

    $('#gPlusButton').on('click', signInClick);
});

var startApp = function() {
    gapi.load('auth2', function(){
    // Retrieve the singleton for the GoogleAuth library and setup the client.
    gapi.auth2.init({
        client_id: '48655955495-io5d8dd1lgb4rghnb1f8fk0emphicpa4.apps.googleusercontent.com',
        cookiepolicy: 'single_host_origin',
        fetch_basic_profile: true,
        scope: 'https://www.googleapis.com/auth/plus.login email https://www.googleapis.com/auth/calendar',
      }).then(function (){
            console.log('init');
            auth2 = gapi.auth2.getAuthInstance();
            auth2.then(function() {
                var isAuthedCallback = function () {
                  onSignInCallback(auth2.currentUser.get().getAuthResponse())
                }
                //helper.people(isAuthedCallback);
              });
          });
  });
}

var helper = (function() {
  var authResult = undefined;
  return {
    /**
     * Hides the sign-in button and connects the server-side app after
     * the user successfully signs in.
     *
     * @param {Object} authResult An Object which contains the access token and
     *   other authentication information.
     */
    onSignInCallback: function(authResult) {
      $('#authResult').html('Auth Result:<br/>');
      for (var field in authResult) {
        $('#authResult').append(' ' + field + ': ' + authResult[field] + '<br/>');
      }
      if (authResult['access_token']) {
        // The user is signed in

        this.authResult = authResult;

        // Set the session token and redirect the user

        // After we load the Google+ API, render the profile data from Google+.
        gapi.client.load('plus','v1',this.renderProfile);

        var access_token = authResult['access_token'];
        $.ajax({
            url: '/main/updatetoken',
            type: 'post',
            data: { token: access_token },
            success: function(res) {
                window.location.href = '/main/plans';
            }
        });

        // After we load the profile, retrieve the list of people visible to
        // this app, server-side.
        // helper.people();
      } else if (authResult['error']) {
        // There was an error, which means the user is not signed in.
        // As an example, you can troubleshoot by writing to the console:
        console.log('There was an error: ' + authResult['error']);
        $('#authResult').append('Logged out');
        $('#authOps').hide('slow');
        $('#gConnect').show();
      }
      console.log('authResult', authResult);
    },
    /**
     * Retrieves and renders the authenticated user's Google+ profile.
     */
    renderProfile: function() {
      var request = gapi.client.plus.people.get( {'userId' : 'me'} );
      request.execute(function(profile) {
          $('#profile').empty();
          if (profile.error) {
            $('#profile').append(profile.error);
            return;
          }
          $('#profile').append(
              $('<p><img src=\"' + profile.image.url + '\"></p>'));
          $('#profile').append(
              $('<p>Hello ' + profile.displayName + '!<br />Tagline: ' +
              profile.tagline + '<br />About: ' + profile.aboutMe + '</p>'));
          if (profile.cover && profile.coverPhoto) {
            $('#profile').append(
                $('<p><img src=\"' + profile.cover.coverPhoto.url + '\"></p>'));
          }
        });
      $('#authOps').show('slow');
      $('#gConnect').hide();
    },
    /**
     * Calls the server endpoint to disconnect the app for the user.
     */
    disconnectServer: function() {
      // Revoke the server tokens
      $.ajax({
        type: 'POST',
        url: window.location.href + '/disconnect',
        async: false,
        success: function(result) {
          console.log('revoke response: ' + result);
          $('#authOps').hide();
          $('#profile').empty();
          $('#visiblePeople').empty();
          $('#authResult').empty();
          $('#gConnect').show();
        },
        error: function(e) {
          console.log(e);
        }
      });
    },
    /**
     * Calls the server endpoint to connect the app for the user. The client
     * sends the one-time authorization code to the server and the server
     * exchanges the code for its own tokens to use for offline API access.
     * For more information, see:
     *   https://developers.google.com/+/web/signin/server-side-flow
     */
    connectServer: function(code) {
      console.log(code);
      $.ajax({
        type: 'POST',
        url: window.location.href + '/connect?state={{ STATE }}',
        contentType: 'application/octet-stream; charset=utf-8',
        success: function(result) {
          console.log(result);
          helper.people();
          onSignInCallback(auth2.currentUser.get().getAuthResponse());
        },
        processData: false,
        data: code
      });
    },
    /**
     * Calls the server endpoint to get the list of people visible to this app.
     * @param success Callback called on success.
     * @param failure Callback called on error.
     */
    people: function(success, failure) {
      success = success || function(result) { helper.appendCircled(result); };
      $.ajax({
        type: 'GET',
        url: window.location.href + '/people',
        contentType: 'application/octet-stream; charset=utf-8',
        success: success,
        error: failure,
        processData: false
      });
    },
    /**
     * Displays visible People retrieved from server.
     *
     * @param {Object} people A list of Google+ Person resources.
     */
    appendCircled: function(people) {
      $('#visiblePeople').empty();
      $('#visiblePeople').append('Number of people visible to this app: ' +
          people.totalItems + '<br/>');
      for (var personIndex in people.items) {
        person = people.items[personIndex];
        $('#visiblePeople').append('<img src="' + person.image.url + '">');
      }
    },
  };
})();

var signInClick = function() {
  var signIn = function(result) {
      auth2.signIn().then(
        function(googleUser) {
          onSignInCallback(googleUser.getAuthResponse());
        }, function(error) {
          alert(JSON.stringify(error, undefined, 2));
        });
    };
  var reauthorize = function() {
      auth2.grantOfflineAccess().then(
        function(result){
          helper.connectServer(result.code);
        });
    };
    helper.people(signIn, reauthorize);
}

var onSignInCallback = function(authResult) {
    helper.onSignInCallback(authResult);
}
</script>