<h3>Your Plans</h3>
<table class="table table-hover table-striped">
    <thead>
        <tr>
            <th>What?</th>
            <th>How much?</th>
            <th>Where?</th>
            <th>When?</th>
            <th>Who's going?</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($plans as $plan): ?>
        <tr>
            <td><?php echo $plan->Title ?></td>
            <td>$<?php echo number_format($plan->PlanCost, 2, '.', ',') ?></td>
            <td><?php echo $plan->LocationName ?></td>
            <td><?php echo $plan->PlanTime ?></td>
            <td><?php foreach ($plan['PlanParticipants'] as $participant): ?>
                <?php echo $participant->Name ?>
                <?php if ($plan->Creator->Id == $participant->Id): ?>
                <button class="btn btn-xs btn-default" style="font-size: 70%">organiser</button>
                <?php endif; ?>
                <br />
            <?php endforeach; ?></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<div class="buttons">
    <button id="new-plan-btn" class="btn btn-primary">New Plan</button>
</div>

<br />

<h4>What do I need to do?</h4>
<table class="table table-hover table-striped">
    <tbody>
        <?php $numToDo = 0; ?>
        <?php foreach ($plans as $plan): ?>
            <?php if ($plan->Creator->Id == $currentUser->Id): continue; endif; ?>

            <?php foreach ($plan['PlanParticipants'] as $participant): ?>
                <?php if ($participant->Id != $plan->UserId
                          && $participant->_joinData->Paid != 1
                          && doubleval($participant->_joinData->SplitAmount) > 0):
                    $numToDo++;
                ?>

                    <?php
                        $paymentLink =
                            'https://venmo.com/?txn=pay&recipients=' . urlencode($plan->Creator->Email)
                            . '&amount=' . urlencode(doubleval($participant->_joinData->SplitAmount))
                            . '&note=' . urlencode('Payment for ' . $plan->Title) . '&audience=public';

                    ?>
                    <tr>
                        <td>
                            <a href="<?php echo $paymentLink ?>">Pay <strong><?php echo $plan->Creator->Name ?></strong> $<?php echo $participant->_joinData->SplitAmount ?> for <strong><?php echo $plan->Title ?></strong></a>.
                        </td>
                    </tr>
                <?php endif; ?>
            <?php endforeach; ?>
        <?php endforeach; ?>

        <?php if ($numToDo == 0): ?>
        <tr>
            <td>You're all set!</td>
        </tr>
        <?php endif ?>
    </tbody>
</table>

<script type="text/javascript">
$(document).ready(function() {
    $('#new-plan-btn').on('click', function() {
        window.location.href = '/main/planner';
    });
});
</script>